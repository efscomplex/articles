const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
     entry: './src/index.js',
     output:{
          path:path.resolve(__dirname,'./dist'),
     },
     module:{
          rules:[
               {
                    test:/.jpe?g|.png|gif/i,
                    loader:'file-loader',
                    options:{
                         outputPath:'assets',
                         name:'[name].[ext]'
                    }
               },
               {
                    test:/.pug$/i,
                    use:[
                         {
                              loader:'pug-loader',
                              options:{ self: true }
                         }
                    ]
               },
               {
                    test:/.s?css$/i,
                    use:[
                         'style-loader',
                         {
                              loader:'css-loader',
                              options: {
                                   sourceMap: true,
                              }
                         },
                         {
                              loader:'sass-loader',
                              options: {
                                   sassOptions:{
                                       includePaths: [path.resolve(__dirname, './src/scss')]
                                   }
                              }
                         }
                    ]
               }
          ]
     },
     plugins: [new HtmlWebpackPlugin({
          template:"src/index.pug",
     })]
}