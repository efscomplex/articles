export class Form {
    constructor(id="form"){
        this.id = id
        this.fullPath = '/'
        this.setData()
        this.addListeners()
    }
    setData(){
      this.data = { }
    }
    get el(){
        return document.getElementById(this.id)
    }
    onSubmit(event){
        event.preventDefault()
        let formData = new FormData(this.el)
        let entries = new Map(formData.entries())
        let data = Object.fromEntries(entries)
        this.updateData(data)
        this.cleanData()
        document.dispatchEvent(new CustomEvent('updatedFormData'))
    }
    onChange(event){
        if (event.detail.type === "file"){
            this.fullPath = event.detail.value
        }
    }
    addListeners(){
        this.el.addEventListener('submit',this.onSubmit.bind(this))
        this.el.addEventListener('inputChange',this.onChange.bind(this))  
    }
    updateData(data){
        this.data = data
        this.data.fullPath = this.fullPath
    }
    cleanData(){
        this.el.reset()
    }
}