export default class CardFromTemplate {
    constructor(id="card"){
        this.template = null
        this.el = document.getElementById(id)
        if (this.el){
            this.template = this.el.content.children[0].cloneNode(true)
        }
    }
    setCardTemplate({title,body,fullPath}){
        if(!this.template || !this.el) return

        let cardHeader = this.template.querySelector('.card-header')
        let cardBody = this.template.querySelector('.card-body')
        let cardImg = this.template.querySelector('.card-img')
        
        cardHeader.innerHTML = `<h1 class="title">${title}</h1>`
        cardBody.innerHTML = `<p>${body}</p>`
        cardImg.src = fullPath
    }
}