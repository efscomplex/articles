import CardFromDomTemplate from './js/card'
import {Form} from './js/formController'
import './scss/style.scss'

document.addEventListener('DOMContentLoaded',()=>{
    initApp()
}) 
function initApp(){
    let form = new Form('formArticle') 
    document.addEventListener('updatedFormData', ()=>{
        let articleStore = document.getElementById('cardStore')
        let card = new CardFromDomTemplate()
        card.setCardTemplate(form.data)
        articleStore.appendChild(card.template)  
    })
}


