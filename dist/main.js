/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./src/scss/style.scss":
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/sass-loader/dist/cjs.js??ref--6-2!./src/scss/style.scss ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\")(true);\n// Module\nexports.push([module.i, \"._2ybsoctzpk6pMkS0ZIBOW2 {\\n  width: 100%; }\\n\\n._3QSWgeNMHbeSJGXfnrYivB, ._2XZaH0Ah-V1I-JNHO1ZCY_ {\\n  padding: 2rem 1rem;\\n  margin-bottom: 2rem;\\n  background-color: #303030;\\n  color: #fff;\\n  border-radius: 0.3rem; }\\n  @media screen and (min-width: 576px) {\\n    ._3QSWgeNMHbeSJGXfnrYivB, ._2XZaH0Ah-V1I-JNHO1ZCY_ {\\n      padding: 4rem 2rem; } }\\n\\n._2XZaH0Ah-V1I-JNHO1ZCY_ {\\n  margin: 1rem; }\\n\\n._14vBspVzwH5RR5rbN3uXnA, [class*=\\\"wrapper-\\\"], .P2sLLvzYlxE5JmuoSnofM {\\n  padding: 1rem;\\n  display: flex;\\n  flex-wrap: wrap;\\n  align-items: center; }\\n\\n.P2sLLvzYlxE5JmuoSnofM {\\n  justify-content: center; }\\n\\n._2Bsq-Oe_JnUBE2QouumVgD {\\n  justify-content: space-evenly; }\\n\\n._17a5Q01azYz036-J4HqRYd {\\n  justify-content: space-between; }\\n\\n._21X624Jrr6REOo0nO7i1dK {\\n  display: grid; }\\n\\n._2OB_w7pBgcvbXjIB6PZhH0 {\\n  max-width: 100%;\\n  object-fit: cover;\\n  object-position: center; }\\n\\n._1a5qGl-TP4P0MMHT5XSAfU {\\n  background-size: cover;\\n  background-position: center;\\n  background-repeat: no-repeat; }\\n\\n._3Ikp5tRsvpbzwrn9AOScLE {\\n  border-radius: 3rem;\\n  padding: 1rem; }\\n\\n._3zRHVkrLgSdzEqfoVW9gPU {\\n  overflow: hidden;\\n  max-height: 0;\\n  opacity: 0;\\n  transition: all .4s ease; }\\n\\nbody {\\n  --primary:#375A7F;\\n  --secondary:#444444;\\n  --text:#FFFFFF;\\n  --info:#3498DB;\\n  --warning:#F39C12;\\n  --danger:#222222;\\n  --success:#00BC8C; }\\n  body html {\\n    display: flex; }\\n    body html ::-webkit-scrollbar {\\n      width: 0px;\\n      background: transparent;\\n      height: 0; }\\n  body body {\\n    margin: 0;\\n    width: 100vw;\\n    overflow-x: hidden; }\\n    body body ul {\\n      list-style: none;\\n      padding: 0; }\\n    body body a {\\n      color: inherit;\\n      text-decoration: none; }\\n  body *, body *:before, body *:after {\\n    box-sizing: border-box; }\\n  body * {\\n    margin: 0; }\\n  body * {\\n    padding: 0; }\\n\\nbody {\\n  min-height: 100vh;\\n  display: grid;\\n  grid-template-areas: 'header'  'main'  'footer';\\n  grid-template-rows: fit-content(0) 1fr fit-content(0); }\\n  body .KKFesD08V9Tm9yw7WxJPA {\\n    grid-area: header; }\\n  body ._1KprdoEeN1FIpdntMt1Akz {\\n    grid-area: main; }\\n  body ._3gsWsfsmR5bQ-CvfsSNVH5 {\\n    grid-area: footer; }\\n  body .KKFesD08V9Tm9yw7WxJPA {\\n    padding: 1rem 5vw; }\\n\", \"\",{\"version\":3,\"sources\":[\"style.scss\"],\"names\":[],\"mappings\":\"AAAA;EACE,WAAW,EAAE;;AAEf;EACE,kBAAkB;EAClB,mBAAmB;EACnB,yBAAyB;EACzB,WAAW;EACX,qBAAqB,EAAE;EACvB;IACE;MACE,kBAAkB,EAAE,EAAE;;AAE5B;EACE,YAAY,EAAE;;AAEhB;EACE,aAAa;EACb,aAAa;EACb,eAAe;EACf,mBAAmB,EAAE;;AAEvB;EACE,uBAAuB,EAAE;;AAE3B;EACE,6BAA6B,EAAE;;AAEjC;EACE,8BAA8B,EAAE;;AAElC;EACE,aAAa,EAAE;;AAEjB;EACE,eAAe;EACf,iBAAiB;EACjB,uBAAuB,EAAE;;AAE3B;EACE,sBAAsB;EACtB,2BAA2B;EAC3B,4BAA4B,EAAE;;AAEhC;EACE,mBAAmB;EACnB,aAAa,EAAE;;AAEjB;EACE,gBAAgB;EAChB,aAAa;EACb,UAAU;EACV,wBAAwB,EAAE;;AAE5B;EACE,iBAAiB;EACjB,mBAAmB;EACnB,cAAc;EACd,cAAc;EACd,iBAAiB;EACjB,gBAAgB;EAChB,iBAAiB,EAAE;EACnB;IACE,aAAa,EAAE;IACf;MACE,UAAU;MACV,uBAAuB;MACvB,SAAS,EAAE;EACf;IACE,SAAS;IACT,YAAY;IACZ,kBAAkB,EAAE;IACpB;MACE,gBAAgB;MAChB,UAAU,EAAE;IACd;MACE,cAAc;MACd,qBAAqB,EAAE;EAC3B;IACE,sBAAsB,EAAE;EAC1B;IACE,SAAS,EAAE;EACb;IACE,UAAU,EAAE;;AAEhB;EACE,iBAAiB;EACjB,aAAa;EACb,+CAA+C;EAC/C,qDAAqD,EAAE;EACvD;IACE,iBAAiB,EAAE;EACrB;IACE,eAAe,EAAE;EACnB;IACE,iBAAiB,EAAE;EACrB;IACE,iBAAiB,EAAE\",\"file\":\"style.scss\",\"sourcesContent\":[\".container {\\n  width: 100%; }\\n\\n.jumbotron, .decorator {\\n  padding: 2rem 1rem;\\n  margin-bottom: 2rem;\\n  background-color: #303030;\\n  color: #fff;\\n  border-radius: 0.3rem; }\\n  @media screen and (min-width: 576px) {\\n    .jumbotron, .decorator {\\n      padding: 4rem 2rem; } }\\n\\n.decorator {\\n  margin: 1rem; }\\n\\n.wrap, [class*=\\\"wrapper-\\\"], .wrapper {\\n  padding: 1rem;\\n  display: flex;\\n  flex-wrap: wrap;\\n  align-items: center; }\\n\\n.wrapper {\\n  justify-content: center; }\\n\\n.wrapper-f_se {\\n  justify-content: space-evenly; }\\n\\n.wrapper-f_sb {\\n  justify-content: space-between; }\\n\\n.grid {\\n  display: grid; }\\n\\n.img {\\n  max-width: 100%;\\n  object-fit: cover;\\n  object-position: center; }\\n\\n.bg-img {\\n  background-size: cover;\\n  background-position: center;\\n  background-repeat: no-repeat; }\\n\\n.pill {\\n  border-radius: 3rem;\\n  padding: 1rem; }\\n\\n.hidden {\\n  overflow: hidden;\\n  max-height: 0;\\n  opacity: 0;\\n  transition: all .4s ease; }\\n\\nbody {\\n  --primary:#375A7F;\\n  --secondary:#444444;\\n  --text:#FFFFFF;\\n  --info:#3498DB;\\n  --warning:#F39C12;\\n  --danger:#222222;\\n  --success:#00BC8C; }\\n  body html {\\n    display: flex; }\\n    body html ::-webkit-scrollbar {\\n      width: 0px;\\n      background: transparent;\\n      height: 0; }\\n  body body {\\n    margin: 0;\\n    width: 100vw;\\n    overflow-x: hidden; }\\n    body body ul {\\n      list-style: none;\\n      padding: 0; }\\n    body body a {\\n      color: inherit;\\n      text-decoration: none; }\\n  body *, body *:before, body *:after {\\n    box-sizing: border-box; }\\n  body * {\\n    margin: 0; }\\n  body * {\\n    padding: 0; }\\n\\nbody {\\n  min-height: 100vh;\\n  display: grid;\\n  grid-template-areas: 'header'  'main'  'footer';\\n  grid-template-rows: fit-content(0) 1fr fit-content(0); }\\n  body .header {\\n    grid-area: header; }\\n  body .main {\\n    grid-area: main; }\\n  body .footer {\\n    grid-area: footer; }\\n  body .header {\\n    padding: 1rem 5vw; }\\n\"]}]);\n// Exports\nexports.locals = {\n\t\"container\": \"_2ybsoctzpk6pMkS0ZIBOW2\",\n\t\"jumbotron\": \"_3QSWgeNMHbeSJGXfnrYivB\",\n\t\"decorator\": \"_2XZaH0Ah-V1I-JNHO1ZCY_\",\n\t\"wrap\": \"_14vBspVzwH5RR5rbN3uXnA\",\n\t\"wrapper\": \"P2sLLvzYlxE5JmuoSnofM\",\n\t\"wrapper-f_se\": \"_2Bsq-Oe_JnUBE2QouumVgD\",\n\t\"wrapper-f_sb\": \"_17a5Q01azYz036-J4HqRYd\",\n\t\"grid\": \"_21X624Jrr6REOo0nO7i1dK\",\n\t\"img\": \"_2OB_w7pBgcvbXjIB6PZhH0\",\n\t\"bg-img\": \"_1a5qGl-TP4P0MMHT5XSAfU\",\n\t\"pill\": \"_3Ikp5tRsvpbzwrn9AOScLE\",\n\t\"hidden\": \"_3zRHVkrLgSdzEqfoVW9gPU\",\n\t\"header\": \"KKFesD08V9Tm9yw7WxJPA\",\n\t\"main\": \"_1KprdoEeN1FIpdntMt1Akz\",\n\t\"footer\": \"_3gsWsfsmR5bQ-CvfsSNVH5\"\n};\n\n//# sourceURL=webpack:///./src/scss/style.scss?./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/sass-loader/dist/cjs.js??ref--6-2");

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n/*\n  MIT License http://www.opensource.org/licenses/mit-license.php\n  Author Tobias Koppers @sokra\n*/\n// css base code, injected by the css-loader\n// eslint-disable-next-line func-names\nmodule.exports = function (useSourceMap) {\n  var list = []; // return the list of modules as css string\n\n  list.toString = function toString() {\n    return this.map(function (item) {\n      var content = cssWithMappingToString(item, useSourceMap);\n\n      if (item[2]) {\n        return \"@media \".concat(item[2], \"{\").concat(content, \"}\");\n      }\n\n      return content;\n    }).join('');\n  }; // import a list of modules into the list\n  // eslint-disable-next-line func-names\n\n\n  list.i = function (modules, mediaQuery) {\n    if (typeof modules === 'string') {\n      // eslint-disable-next-line no-param-reassign\n      modules = [[null, modules, '']];\n    }\n\n    var alreadyImportedModules = {};\n\n    for (var i = 0; i < this.length; i++) {\n      // eslint-disable-next-line prefer-destructuring\n      var id = this[i][0];\n\n      if (id != null) {\n        alreadyImportedModules[id] = true;\n      }\n    }\n\n    for (var _i = 0; _i < modules.length; _i++) {\n      var item = modules[_i]; // skip already imported module\n      // this implementation is not 100% perfect for weird media query combinations\n      // when a module is imported multiple times with different media queries.\n      // I hope this will never occur (Hey this way we have smaller bundles)\n\n      if (item[0] == null || !alreadyImportedModules[item[0]]) {\n        if (mediaQuery && !item[2]) {\n          item[2] = mediaQuery;\n        } else if (mediaQuery) {\n          item[2] = \"(\".concat(item[2], \") and (\").concat(mediaQuery, \")\");\n        }\n\n        list.push(item);\n      }\n    }\n  };\n\n  return list;\n};\n\nfunction cssWithMappingToString(item, useSourceMap) {\n  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring\n\n  var cssMapping = item[3];\n\n  if (!cssMapping) {\n    return content;\n  }\n\n  if (useSourceMap && typeof btoa === 'function') {\n    var sourceMapping = toComment(cssMapping);\n    var sourceURLs = cssMapping.sources.map(function (source) {\n      return \"/*# sourceURL=\".concat(cssMapping.sourceRoot).concat(source, \" */\");\n    });\n    return [content].concat(sourceURLs).concat([sourceMapping]).join('\\n');\n  }\n\n  return [content].join('\\n');\n} // Adapted from convert-source-map (MIT)\n\n\nfunction toComment(sourceMap) {\n  // eslint-disable-next-line no-undef\n  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));\n  var data = \"sourceMappingURL=data:application/json;charset=utf-8;base64,\".concat(base64);\n  return \"/*# \".concat(data, \" */\");\n}\n\n//# sourceURL=webpack:///./node_modules/css-loader/dist/runtime/api.js?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar stylesInDom = {};\n\nvar isOldIE = function isOldIE() {\n  var memo;\n  return function memorize() {\n    if (typeof memo === 'undefined') {\n      // Test for IE <= 9 as proposed by Browserhacks\n      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805\n      // Tests for existence of standard globals is to allow style-loader\n      // to operate correctly into non-standard environments\n      // @see https://github.com/webpack-contrib/style-loader/issues/177\n      memo = Boolean(window && document && document.all && !window.atob);\n    }\n\n    return memo;\n  };\n}();\n\nvar getTarget = function getTarget() {\n  var memo = {};\n  return function memorize(target) {\n    if (typeof memo[target] === 'undefined') {\n      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself\n\n      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {\n        try {\n          // This will throw an exception if access to iframe is blocked\n          // due to cross-origin restrictions\n          styleTarget = styleTarget.contentDocument.head;\n        } catch (e) {\n          // istanbul ignore next\n          styleTarget = null;\n        }\n      }\n\n      memo[target] = styleTarget;\n    }\n\n    return memo[target];\n  };\n}();\n\nfunction listToStyles(list, options) {\n  var styles = [];\n  var newStyles = {};\n\n  for (var i = 0; i < list.length; i++) {\n    var item = list[i];\n    var id = options.base ? item[0] + options.base : item[0];\n    var css = item[1];\n    var media = item[2];\n    var sourceMap = item[3];\n    var part = {\n      css: css,\n      media: media,\n      sourceMap: sourceMap\n    };\n\n    if (!newStyles[id]) {\n      styles.push(newStyles[id] = {\n        id: id,\n        parts: [part]\n      });\n    } else {\n      newStyles[id].parts.push(part);\n    }\n  }\n\n  return styles;\n}\n\nfunction addStylesToDom(styles, options) {\n  for (var i = 0; i < styles.length; i++) {\n    var item = styles[i];\n    var domStyle = stylesInDom[item.id];\n    var j = 0;\n\n    if (domStyle) {\n      domStyle.refs++;\n\n      for (; j < domStyle.parts.length; j++) {\n        domStyle.parts[j](item.parts[j]);\n      }\n\n      for (; j < item.parts.length; j++) {\n        domStyle.parts.push(addStyle(item.parts[j], options));\n      }\n    } else {\n      var parts = [];\n\n      for (; j < item.parts.length; j++) {\n        parts.push(addStyle(item.parts[j], options));\n      }\n\n      stylesInDom[item.id] = {\n        id: item.id,\n        refs: 1,\n        parts: parts\n      };\n    }\n  }\n}\n\nfunction insertStyleElement(options) {\n  var style = document.createElement('style');\n\n  if (typeof options.attributes.nonce === 'undefined') {\n    var nonce =  true ? __webpack_require__.nc : undefined;\n\n    if (nonce) {\n      options.attributes.nonce = nonce;\n    }\n  }\n\n  Object.keys(options.attributes).forEach(function (key) {\n    style.setAttribute(key, options.attributes[key]);\n  });\n\n  if (typeof options.insert === 'function') {\n    options.insert(style);\n  } else {\n    var target = getTarget(options.insert || 'head');\n\n    if (!target) {\n      throw new Error(\"Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.\");\n    }\n\n    target.appendChild(style);\n  }\n\n  return style;\n}\n\nfunction removeStyleElement(style) {\n  // istanbul ignore if\n  if (style.parentNode === null) {\n    return false;\n  }\n\n  style.parentNode.removeChild(style);\n}\n/* istanbul ignore next  */\n\n\nvar replaceText = function replaceText() {\n  var textStore = [];\n  return function replace(index, replacement) {\n    textStore[index] = replacement;\n    return textStore.filter(Boolean).join('\\n');\n  };\n}();\n\nfunction applyToSingletonTag(style, index, remove, obj) {\n  var css = remove ? '' : obj.css; // For old IE\n\n  /* istanbul ignore if  */\n\n  if (style.styleSheet) {\n    style.styleSheet.cssText = replaceText(index, css);\n  } else {\n    var cssNode = document.createTextNode(css);\n    var childNodes = style.childNodes;\n\n    if (childNodes[index]) {\n      style.removeChild(childNodes[index]);\n    }\n\n    if (childNodes.length) {\n      style.insertBefore(cssNode, childNodes[index]);\n    } else {\n      style.appendChild(cssNode);\n    }\n  }\n}\n\nfunction applyToTag(style, options, obj) {\n  var css = obj.css;\n  var media = obj.media;\n  var sourceMap = obj.sourceMap;\n\n  if (media) {\n    style.setAttribute('media', media);\n  }\n\n  if (sourceMap && btoa) {\n    css += \"\\n/*# sourceMappingURL=data:application/json;base64,\".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), \" */\");\n  } // For old IE\n\n  /* istanbul ignore if  */\n\n\n  if (style.styleSheet) {\n    style.styleSheet.cssText = css;\n  } else {\n    while (style.firstChild) {\n      style.removeChild(style.firstChild);\n    }\n\n    style.appendChild(document.createTextNode(css));\n  }\n}\n\nvar singleton = null;\nvar singletonCounter = 0;\n\nfunction addStyle(obj, options) {\n  var style;\n  var update;\n  var remove;\n\n  if (options.singleton) {\n    var styleIndex = singletonCounter++;\n    style = singleton || (singleton = insertStyleElement(options));\n    update = applyToSingletonTag.bind(null, style, styleIndex, false);\n    remove = applyToSingletonTag.bind(null, style, styleIndex, true);\n  } else {\n    style = insertStyleElement(options);\n    update = applyToTag.bind(null, style, options);\n\n    remove = function remove() {\n      removeStyleElement(style);\n    };\n  }\n\n  update(obj);\n  return function updateStyle(newObj) {\n    if (newObj) {\n      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {\n        return;\n      }\n\n      update(obj = newObj);\n    } else {\n      remove();\n    }\n  };\n}\n\nmodule.exports = function (list, options) {\n  options = options || {};\n  options.attributes = typeof options.attributes === 'object' ? options.attributes : {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>\n  // tags it will allow on a page\n\n  if (!options.singleton && typeof options.singleton !== 'boolean') {\n    options.singleton = isOldIE();\n  }\n\n  var styles = listToStyles(list, options);\n  addStylesToDom(styles, options);\n  return function update(newList) {\n    var mayRemove = [];\n\n    for (var i = 0; i < styles.length; i++) {\n      var item = styles[i];\n      var domStyle = stylesInDom[item.id];\n\n      if (domStyle) {\n        domStyle.refs--;\n        mayRemove.push(domStyle);\n      }\n    }\n\n    if (newList) {\n      var newStyles = listToStyles(newList, options);\n      addStylesToDom(newStyles, options);\n    }\n\n    for (var _i = 0; _i < mayRemove.length; _i++) {\n      var _domStyle = mayRemove[_i];\n\n      if (_domStyle.refs === 0) {\n        for (var j = 0; j < _domStyle.parts.length; j++) {\n          _domStyle.parts[j]();\n        }\n\n        delete stylesInDom[_domStyle.id];\n      }\n    }\n  };\n};\n\n//# sourceURL=webpack:///./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _scss_style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./scss/style.scss */ \"./src/scss/style.scss\");\n/* harmony import */ var _scss_style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_scss_style_scss__WEBPACK_IMPORTED_MODULE_0__);\n\n\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "./src/scss/style.scss":
/*!*****************************!*\
  !*** ./src/scss/style.scss ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../node_modules/sass-loader/dist/cjs.js??ref--6-2!./style.scss */ \"./node_modules/css-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./src/scss/style.scss\");\n\nif (typeof content === 'string') {\n  content = [[module.i, content, '']];\n}\n\nvar options = {}\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = __webpack_require__(/*! ../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\")(content, options);\n\nif (content.locals) {\n  module.exports = content.locals;\n}\n\n\n//# sourceURL=webpack:///./src/scss/style.scss?");

/***/ })

/******/ });